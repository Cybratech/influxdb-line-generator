package com.cybratech;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) throws IOException {

        String[] pizzaTypes = new String[]{"Salami", "Hawaii", "Schinken", "Vegetaria", "QuattroFormaggi","Marinara"};
        String[] locations = new String[]{"London","Berlin","Paris","Bruessel","Stockholm","Amsterdam"};
        /*Time Lmits*/
        long offset = Timestamp.valueOf("2020-10-01 00:00:00").getTime(); /*First of October*/
        long end = new Timestamp (System.currentTimeMillis()).getTime();
        long diff = end - offset + 1;

        BufferedWriter writer = new BufferedWriter(new FileWriter("pizzadata.line"));
        for (int i = 0; i < 10000; i++) {
            int randomNum = ThreadLocalRandom.current().nextInt(0, pizzaTypes.length );
            String pizzaType = pizzaTypes[randomNum];
            int randomLocation = ThreadLocalRandom.current().nextInt(0, locations.length );
            String location = locations[randomLocation];
            StringBuilder builder = new StringBuilder();
            Timestamp rand = new Timestamp(offset + (long)(Math.random() * diff));
            builder.append("prep,").append("location=").append(surround(location))
                    .append(",type=").append(surround(pizzaType))
                    .append(" preparationTime=").append(ThreadLocalRandom.current().nextInt(5, 21)).append("i")
                    .append(" ").append(rand.getTime() * 1000000);
            System.out.println(builder.toString());
            writer.write(builder.toString());
            writer.newLine();
            builder = new StringBuilder();
            builder.append("extras,").append("location=").append(surround(location))
                    .append(",type=").append(surround(pizzaType))
                    .append(" numExtra=").append(ThreadLocalRandom.current().nextInt(0, 4)).append("i")
                    .append(" ").append(rand.getTime() * 1000000);
            writer.write(builder.toString());
            writer.newLine();
        }
        writer.close();
    }

    private static String surround(String s){
        return "\""+s+"\"";
    }
}
